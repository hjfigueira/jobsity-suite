# Jobsity Services Suite - Bank Chatbot

This repositoty contains the services needed to run the test project in php, as described by the PDF file

# Setup

All tasks can be found in the **./setup.sh** file.

## Requirements

- Docker
- docker-compose
- Ports 80 and 8000 free for use

This project is intended to be used as a showcase, because of that the code is built inside the containers and then 
copied to volumes to be acessible in the host machine. 

The code is not meant to be editable, since nor permissions or changes to the code will be reflected in the container.
(This setup is inspired of what would look like an implementation for server, but making the code available to the host machine)

## Project

This project is composed by 2 separated applications and 3 services.

### Composition
*services*
- PHP 7.4 - Nginx
- Node 13
- Mysql 8
 
 
*applications*

- Client side - ReactJS client application
- Server side - PHP Lumen RestFull API (that depends on Mysql service)

### Commands and Interaction

The bot will make available the set fo commands described bellow
- login  - Asks user for email and password
- logoff - current user session
- signup - Allow users to create accouns
- withdraw - Asks for value to withdraw (currency code as optional)
- deposit - Asks for value to deposit (currency code as optional)
- balance - Show to the user his current balance, in default currency

### Testing

When all services are up, the following command will output test results
```
docker-compose exec api /app/vendor/bin/phpunit
```

## Repos

The applications are built independently, so there are 2 repositories, one for each.

https://gitlab.com/hjfigueira/jobsity-client

https://gitlab.com/hjfigueira/jobsity-api

Thanks for the opportunity and have a nice day :)
