docker-compose build --no-cache
docker-compose up -d

echo 'Waiting 20 seconds for starting the services inside containers'
sleep 20
docker-compose exec api php artisan migrate

#command used just to extract the source code
docker-compose exec api cp -a /app/. /src/
docker-compose exec web cp -a /app/. /src/
